namespace Poski_Test.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Desc = c.String(),
                        Logo = c.Guid(nullable: false),
                        IsAvailable = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Photos",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.Guid(nullable: false),
                        Service_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Services", t => t.Service_ID)
                .Index(t => t.Service_ID);
            
            CreateTable(
                "dbo.Requests",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        VisitorName = c.String(),
                        VisitorEmail = c.String(),
                        VisitorPhone = c.String(),
                        Desc = c.String(),
                        Read = c.Boolean(nullable: false),
                        Service_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Services", t => t.Service_ID)
                .Index(t => t.Service_ID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Requests", new[] { "Service_ID" });
            DropIndex("dbo.Photos", new[] { "Service_ID" });
            DropForeignKey("dbo.Requests", "Service_ID", "dbo.Services");
            DropForeignKey("dbo.Photos", "Service_ID", "dbo.Services");
            DropTable("dbo.Requests");
            DropTable("dbo.Photos");
            DropTable("dbo.Services");
        }
    }
}
