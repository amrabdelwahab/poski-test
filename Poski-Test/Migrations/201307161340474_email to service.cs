namespace Poski_Test.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class emailtoservice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Services", "Email", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Services", "Email");
        }
    }
}
