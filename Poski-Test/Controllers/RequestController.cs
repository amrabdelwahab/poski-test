﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Poski_Test.Models;
using System.Net;
using System.Net.Mail;
namespace Poski_Test.Controllers
{
    public class RequestController : Controller
    {
        public static bool SendEmail(string SentTo, string Text)
        {
            MailMessage mail = new MailMessage();
            mail.To.Add(SentTo);
            mail.From = new MailAddress("poski@support.com");
            mail.Subject = "Quote Request";


            mail.Body = Text;

            mail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
            smtp.Credentials = new System.Net.NetworkCredential
                 ("poskitask@gmail.com", "Windows.2000");
            //Or your Smtp Email ID and Password
            smtp.EnableSsl = true;
            smtp.Send(mail);
            return true;
        }
        //
        // GET: /Request/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RequestCreate(int ServiceID)
        {

            using (db db = new db())
            {
                var rService = db.Services.Find(ServiceID);
                var model = new ViewModels.Request { 
                ServiceID=rService.ID,
                ServiceName=rService.Name
                };

                return View(model);

            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult RequestCreate(ViewModels.Request aRequest,int ServiceID)
        {

            using (var db = new db())
            {
               Service rService = new Service();
                if (ModelState.IsValid)
                {
                    rService = db.Services.Find(ServiceID);
                    Request rRequest = new Request
                    {
                        VisitorEmail=aRequest.VisitorEmail,
                        VisitorName=aRequest.VisitorName,
                        VisitorPhone=aRequest.VisitorPhone,
                        Read=false,
                        Service=rService,
                        Desc=aRequest.Desc
                    };
                   
                    db.Requests.Add(rRequest);
                    db.SaveChanges();
                    var body = "<h1>Request to " + rService.Name + "</h1><h3>User Details</h3><ul><li>Name: " + aRequest.VisitorName + "</li><li>Phone: " + aRequest.VisitorPhone + "</li><li>Email: " + aRequest.VisitorEmail + "</li></ul><h3>Request Body</h3>"+aRequest.Desc;
                    SendEmail(aRequest.VisitorEmail, body);
                    SendEmail(rService.Email, body);
                   
                    return RedirectToAction("ViewService", "Home", new {ServiceID=ServiceID });
                }
                else
                {

                    var errors = ModelState
                        .Where(x => x.Value.Errors.Count > 0)
                        .Select(x => new { x.Key, x.Value.Errors })
                        .ToArray();

                } 
                rService = db.Services.Find(ServiceID);
                var model = new ViewModels.Request
                {
                    ServiceID = rService.ID,
                    ServiceName = rService.Name
                };
                return View( model);
            }

        }
    }
}
