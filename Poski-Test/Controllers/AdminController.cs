﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Poski_Test.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Net.Mail;
namespace Poski_Test.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        //
        // GET: /Admin/

        public ActionResult Index()
        {
            return View();
        }
        #region Request
        
        public ActionResult RequestList()
        {
            using (var db = new db())
            {


                var Request = (from b in db.Requests
                                select new ViewModels.Request
                                {
                                    ID = b.ID,
                                    VisitorName = b.VisitorName,
                                    VisitorPhone = b.VisitorPhone,
                                    VisitorEmail = b.VisitorEmail,
                                    ServiceID=b.Service.ID,
                                    ServiceName=b.Service.Name,
                                    Desc=b.Desc,
                                    Read = b.Read

                                }
                              ).ToList();

                return View(Request);



            }
        }

        public ActionResult RequestListAjax([DataSourceRequest] DataSourceRequest request)
        {
            return Json(GetRequests().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        private static IEnumerable<ViewModels.Request> GetRequests()
        {
            var db = new db();

            return db.Requests.Select(b => new ViewModels.Request
            {
                ID = b.ID,
                VisitorName = b.VisitorName,
                VisitorPhone = b.VisitorPhone,
                VisitorEmail = b.VisitorEmail,
                ServiceID = b.Service.ID,
                ServiceName = b.Service.Name,
                Desc = b.Desc,
                Read = b.Read,
            });
        }


        public ActionResult MarkRead(int Request_id)
        {
            var db = new db();
            var aRequest = db.Requests.Find(Request_id);
            aRequest.Read = true;
            db.SaveChanges();
            return new EmptyResult();
        }
        public ActionResult GetUnread()
        {
            var db = new db();
            int n = db.Requests.Where(f => !f.Read).Count();
            return Json(new { Num = n }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Servicees CRUD
        public ActionResult ServiceList()
        {
            using (var db = new db())
            {


                var Servicees = (from b in db.Services
                                select new ViewModels.Service
                                {
                                    ID = b.ID,
                                    Name = b.Name,

                                }
                              ).ToList();

                return View("Service/List", Servicees);



            }
        }
        public ActionResult ServiceListAjax([DataSourceRequest] DataSourceRequest request)
        {
            return Json(GetServices().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        private static IEnumerable<ViewModels.Service> GetServices()
        {
            var db = new db();

            return db.Services.Select(sp => new ViewModels.Service
            {
                ID = sp.ID,
                Name = sp.Name
            });
        }

        public ActionResult GetPhotos(int ServiceID) { 
        using (db db = new db())
            {
        
      var model=  from P in db.Services.Find(ServiceID).Photos
                            select new ViewModels.Photo
                            {ID=P.ID,
                                Name=P.Name
                            };
               return Json(new { data =model }, JsonRequestBehavior.AllowGet);
            
        }}
       
        public ActionResult ServicePhotos(int ServiceID)
        {

            using (db db = new db())
            {
                var aService = db.Services.Find(ServiceID);
                var model = new ViewModels.Service
                {
                    ID = aService.ID,
                    Name = aService.Name
                };
                            
                return View("Service/Photos", model);

            }
        }
        public ActionResult PhotoDelete(int PhotoID)
        {
            var db = new db();
            var r = db.Photos.Find(PhotoID);
            db.Photos.Remove(r);
            db.SaveChanges();
            return new EmptyResult();
        }
        public ActionResult PhotoCreate(ViewModels.Photo aPhoto, int ServiceID)
        {
            var db = new db();
            var s = db.Services.Find(ServiceID);
            if (aPhoto.PhotoFile != null)
            {
                var rPhoto = new Photo
                {
                    Name = Guid.NewGuid(),
                    Service = s
                };
                db.Photos.Add(rPhoto);
                db.SaveChanges();
                aPhoto.PhotoFile.SaveAs(HttpContext.Server.MapPath("~/Images/Photos/") + rPhoto.Name + ".png");
            }
            return new EmptyResult();
        }
        public ActionResult ServiceCreate()
        {

            using (db db = new db())
            {
                var model = new ViewModels.Service();
                
                return View("Service/Create", model);

            }
        }
 
        [HttpPost]
        [ValidateInput(false)]    
        public ActionResult ServiceCreate(ViewModels.Service aService)
        {

            using (var db = new db())
            {

                if (ModelState.IsValid)
                {

                    Service rService = new Service
                    {
                        Name = aService.Name,
                        Desc=aService.Desc,
                        IsAvailable=aService.isAvailable,
                        Email=aService.Email
                    };
                    if (aService.Logo != null) {

                        rService.Logo = Guid.NewGuid();
                        aService.Logo.SaveAs(HttpContext.Server.MapPath("~/Images/Services/") + rService.Logo + ".png");
                    }
                    db.Services.Add(rService);
                    db.SaveChanges();

                    return RedirectToAction("ServiceList");
                }
                else
                {

                    var errors = ModelState
                        .Where(x => x.Value.Errors.Count > 0)
                        .Select(x => new { x.Key, x.Value.Errors })
                        .ToArray();

                }
                var model = new ViewModels.Service();
                
                return View("Service/Create", model);
            }

        }
        public ActionResult ServiceEdit(int ServiceID)
        {
            using (db db = new db())
            {
                var aService = db.Services.Find(ServiceID);
                var rService = 
                     new ViewModels.Service
                               {ID = aService.ID,
                                Name = aService.Name,
                                Desc = aService.Desc,
                                Email = aService.Email,
                                isAvailable = aService.IsAvailable,
                                LogoName = aService.Logo.ToString()
                               };
                return View("Service/Edit", rService);
            }
        }

        [HttpPost]
        [ValidateInput(false)]  
        public ActionResult ServiceEdit(int ServiceID, ViewModels.Service aService)
        {
            using (db db = new db())
            {
                var rService = db.Services.Find(ServiceID);
                if (ModelState.IsValid)
                {
                    rService.Name = aService.Name;
                    rService.Email = aService.Email ?? "";
                    rService.Desc = aService.Desc ?? "";
                    rService.IsAvailable=aService.isAvailable;
                    if (aService.Logo != null)
                    {

                        rService.Logo = Guid.NewGuid();
                        aService.Logo.SaveAs(HttpContext.Server.MapPath("~/Images/Services/") + rService.Logo + ".png");
                    }
                    db.SaveChanges();

                    return RedirectToAction("ServiceList");
                }
                else
                {

                    var errors = ModelState
                        .Where(x => x.Value.Errors.Count > 0)
                        .Select(x => new { x.Key, x.Value.Errors })
                        .ToArray();

                }

                return View("Service/Edit", aService);
            }


        }

        
        #endregion


    }
}
