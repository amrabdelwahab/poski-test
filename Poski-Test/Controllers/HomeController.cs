﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Poski_Test.Models;

namespace Poski_Test.Controllers
{
    public class HomeController : Controller
    {
         public ActionResult ViewService(int ServiceID){
             var db = new db();
             var rService = db.Services.Find(ServiceID);
             var model = new ViewModels.Service
             {
                 ID = rService.ID,
                 Name = rService.Name,
                 Desc = rService.Desc
             };
             model.Photos=new List<string>();
             foreach (Photo p in rService.Photos) {
                 model.Photos.Add(p.Name.ToString());
             }
             return View(model);
         }

        public ActionResult Index()
        {
            var db = new db();
            var services=(from s in db.Services
                      where s.IsAvailable
                      select new ViewModels.Service{
            ID=s.ID,
            Name=s.Name
            }).AsEnumerable();
            var model = new List<ViewModels.Service>();
            Service rs = new Service();
            var ss = new ViewModels.Service();
            foreach (ViewModels.Service ser in services) {
                rs = db.Services.Find(ser.ID);
                ss = new ViewModels.Service
                {
                    ID = ser.ID,
                    Name = ser.Name,
                    LogoName = rs.Logo.ToString()
                };
                model.Add(ss);

            }
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
