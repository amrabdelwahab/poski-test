﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Poski_Test.Models
{
    public class Service
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Desc { get; set; }
        public Guid Logo { get; set; }
        public Boolean IsAvailable { get; set; }
        public virtual ICollection<Photo> Photos { get; set; }
        public virtual ICollection<Request> Requests { get; set; }
        
    }
}