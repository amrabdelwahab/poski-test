﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Poski_Test.Models
{
    public class Request
    {
        public int ID { get; set; }
        public string VisitorName { get; set; }
        public string VisitorEmail { get; set; }
        public string VisitorPhone { get; set; }
        public string Desc { get; set; }
        public Boolean Read { get; set; }
        public virtual Service Service { get; set; }
         
    }
}