﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Poski_Test.ViewModels
{
    public class Photo
    {

        public Photo()
        {
            
            
        }

        public int ID { get; set; }
        public Guid Name { get; set; }

        public HttpPostedFileBase PhotoFile { get; set; }
    }
}