﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Poski_Test.ViewModels
{
    public class Service
    {

        public Service()
        {
            Name = string.Empty;
            Email = string.Empty;
            Desc = string.Empty;
            isAvailable = false;
        }

        public int ID { get; set; }
        public string Name { get; set; }

        public string Desc { get; set; }
        public HttpPostedFileBase Logo { get; set; }

        public String LogoName { get; set; }
        public string Email { get; set; }
        public Boolean isAvailable { get; set; } 
        public List<string> Photos { get; set; }
        
    }
}