﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Poski_Test.ViewModels
{
    public class Request
    {

        public Request()
        {
            VisitorEmail = string.Empty;
            VisitorName = string.Empty;
            VisitorPhone = string.Empty;
            Desc = string.Empty;
            Read = false;
            ID = 0;
        }

        public int ServiceID { get; set; }
        public string ServiceName { get; set; }
        public int ID { get; set; }
        public string VisitorName { get; set; }
        public string VisitorEmail { get; set; }
        public string VisitorPhone { get; set; }
        public string Desc { get; set; }
        public Boolean Read { get; set; }
    }
}