﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Poski_Test.Models
{
    public class Photo
    {
        public int ID { get; set; }    
        public Guid Name { get; set; }
        public virtual Service Service { get; set; }
         
    }
}