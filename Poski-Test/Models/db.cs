﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Poski_Test.Models
{
    public class db : DbContext
    {
       public db()
        {
            Database.SetInitializer<db>(null);
        }
        public DbSet<Service> Services { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Request> Requests{ get; set; }
    }
}