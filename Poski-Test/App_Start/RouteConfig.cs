﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Poski_Test
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
               name: "ViewRequests",
               url: "requests",
               defaults: new { controller = "Admin", action = "RequestList", id = UrlParameter.Optional }
           );
          
            routes.MapRoute(
               name: "ReadRequest",
               url: "requests/read/{Request_id}",
               defaults: new { controller = "Admin", action = "MarkRead", id = UrlParameter.Optional }
           );
            routes.MapRoute(
                name: "unread",
                url: "requests/unread",
                defaults: new { controller = "Admin", action = "GetUnread", id = UrlParameter.Optional }
            );
            
            routes.MapRoute(
               name: "Admin",
               url: "admin",
               defaults: new { controller = "Admin", action = "Index", id = UrlParameter.Optional }
           );
            routes.MapRoute(
                name: "ServiceCreate",
                url: "services/create",
                defaults: new { controller = "Admin", action = "ServiceCreate", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "ServiceEdit",
                url: "services/edit/{ServiceID}",
                defaults: new { controller = "Admin", action = "ServiceEdit", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "ServiceGetAllPhotos",
                url: "services/photosapi/{ServiceID}",
                defaults: new { controller = "Admin", action = "GetPhotos", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "RequestCreate",
                url: "service/request/{ServiceID}",
                defaults: new { controller = "Request", action = "RequestCreate", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "PhotoCreate",
                url: "photos/create/{ServiceID}",
                defaults: new { controller = "Admin", action = "PhotoCreate", id = UrlParameter.Optional }
            ); routes.MapRoute(
                name: "ServiceDetails",
                url: "services/view/{ServiceID}",
                defaults: new { controller = "Home", action = "ViewService", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Photodelete",
                url: "photos/delete/{PhotoID}",
                defaults: new { controller = "Admin", action = "PhotoDelete", id = UrlParameter.Optional }
            );
            
            routes.MapRoute(
                name: "ServicePhotos",
                url: "services/photos/{ServiceID}",
                defaults: new { controller = "Admin", action = "ServicePhotos", id = UrlParameter.Optional }
            );
            routes.MapRoute(
               name: "ServiceList",
               url: "services",
               defaults: new { controller = "Admin", action = "ServiceList", id = UrlParameter.Optional }
           );
            
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

        }
    }
}